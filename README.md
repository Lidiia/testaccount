## Запуск проекту

Оновлення vendor **composer update**

Запуск міграції **php bin/console doctrine:migrations:migrate**

Запуск серверу черги **docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management**

Запуск воркера **php bin/console messenger:consume ampq**

Перевірити доступ на щапис в папки var/cache та var/log

Все ще є проблема з обробкою помилок, але я над цим працюю