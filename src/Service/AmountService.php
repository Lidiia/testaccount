<?php

namespace App\Service;

use App\Entity\History;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Http\Client\Exception;


class AmountService
{
    /**
     * @var EntityManagerInterface
     */
    private $repository;

    private $dataDraft = array('amount' => '', 'tid' => '', 'uid' => '');

    public function __construct(EntityManagerInterface $repository)
    {
        $this->repository   = $repository;
    }

    public function credit($data)
    {
        $data = $this->checkData($data);

        if($user = $this->repository->getRepository(User::class)->findOneBy(array('uid' => $data['uid']))){
            $this->repository->beginTransaction();
            try{
                $user->setBalance($user->getBalance() + $data['amount'] * 100);
                $this->repository->persist($user);
                $this->repository->flush();

                $history = new History();
                $history->createData($data['tid'], 'credit', $data['amount'] * 100, $user, new \DateTime());
                $this->repository->persist($history);
                $this->repository->flush();

                $this->repository->commit();
                return true;
            } catch (\Exception $e){
                $this->repository->rollback();
                throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);
            }
        }
        throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);
    }

    public function debit($data)
    {
        $data = $this->checkData($data);

        if($user = $this->repository->getRepository(User::class)->findOneBy(array('uid' => $data['uid']))){
            $this->repository->beginTransaction();
            try{
                $user->setBalance($user->getBalance() - $data['amount'] * 100);
                if($user->getBalance() < 0){
                    throw new \Exception("insufficient funds", 200);
                }
                $this->repository->persist($user);
                $this->repository->flush();

                $history = new History();
                $history->createData($data['tid'], 'debit', $data['amount'] * 100 * -1, $user, new \DateTime());
                $this->repository->persist($history);
                $this->repository->flush();

                $this->repository->commit();
                return true;
            } catch (\Exception $e){
                $this->repository->rollback();
                $code = $e->getCode() == 200 ? 200 : 500;
                throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="' . $e->getMessage() . '"></result>', $code);
            }
        }
        throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);
    }

    public function checkData($data)
    {
        $data = array_change_key_case($data);
        if(count(array_diff_key($this->dataDraft, $data)) > 0)
            throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);

        if($this->repository->getRepository(History::class)->findOneBy(array('tid' => $data['tid'])))
            throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);

        return $data;
    }

}