<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200602062022 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, uid VARCHAR(32) NOT NULL, name VARCHAR(255) NOT NULL, balance INT(11) UNSIGNED NOT NULL, INDEX IDX_7D3656A49D86650Y (uid), UNIQUE INDEX `uid_UNIQUE` (`uid` ASC), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE history (id INT AUTO_INCREMENT NOT NULL, tid VARCHAR(32) NOT NULL, type set(\'debit\', \'credit\'), amount INT NOT NULL, user_id INT NOT NULL, date DATETIME NOT NULL, INDEX IDX_27BA704B49CB4726 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704B49CB4726 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('INSERT INTO user (id, uid, name, balance) VALUES (1, "507f1f77bcf86cd799439011", "Anatoliy Muhomorov", 50000);');
        $this->addSql('INSERT INTO user (id, uid, name, balance) VALUES (2, "jkdkz86s693847283hhhndsa", "Lubov Osadcha", 500000);');
        $this->addSql('INSERT INTO user (id, uid, name, balance) VALUES (3, "d8a7ewwqjkejsd907ejwej71", "Vitalii Banduk", 5469000);');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE history DROP FOREIGN KEY FK_27BA704B49CB4726');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE history');
    }
}
