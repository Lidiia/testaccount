<?php

namespace App\MessageHandler;

use App\Entity\History;
use App\Entity\User;
use App\Message\QueueMessage;
use App\Service\AmountService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;


class QueueMessageHandler implements MessageHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $repository;

    /**
     * @var array
     */
    private $dataDraft = array('amount' => '', 'tid' => '', 'uid' => '');

    public function __construct(EntityManagerInterface $repository)
    {
        $this->repository   = $repository;
    }

    public function __invoke(QueueMessage $msg)
    {
//        $a = new AmountService();
//        $a->credit($msg->getData());
//        return;
        $data = array_change_key_case($msg->getData());
        if(count(array_diff_key($this->dataDraft, $data)) > 0)
            throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);

        if($this->repository->getRepository(History::class)->findOneBy(array('tid' => $data['tid'])))
            throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);

        if($user = $this->repository->getRepository(User::class)->findOneBy(array('uid' => $data['uid']))){
            $this->repository->beginTransaction();
            try{
                $balance = $msg->getType() == 'credit' ? $data['amount'] * 100 : $data['amount'] * 100 * -1;
                $user->setBalance($user->getBalance() + $balance);
                if($user->getBalance() < 0){
                    throw new \Exception("insufficient funds", 200);
                }
                $this->repository->persist($user);
                $this->repository->flush();

                $history = new History();
                $history->createData($data['tid'], $msg->getType(), $balance, $user, new \DateTime());
                $this->repository->persist($history);
                $this->repository->flush();

                $this->repository->commit();
                return true;
            } catch (\Exception $e){
                $this->repository->rollback();
                $code = $e->getCode() == 200 ? 200 : 500;
                throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="' . $e->getMessage() . '"></result>', $code);
            }
        }
        throw new \Exception('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);
    }

    public static function getHandledMessages(): iterable
    {
        return '55'; die;
    }
}