<?php

namespace App\Controller;

use App\Message\QueueMessage;
use App\Service\AmountService;
use Assert\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ApiController.php',
        ]);
    }

    /**
     * @Route("/api/changebalance", name="changebalance", methods={"POST"})
     */
    public function changebalance(Request $request, MessageBusInterface $bus)
    {
        $p = xml_parser_create();
        xml_parse_into_struct($p, $request->getContent(), $data);
        xml_parser_free($p);

//        if(!is_array($data) || $data[0]['tag'] != 'CREDIT')
/*            return new Response('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);*/

        try{
//            $service->credit($data[0]['attributes']);
            $t = $bus->dispatch(new QueueMessage($data[0]['attributes'], $data[0]['tag']));
        } catch (InvalidArgumentException $e){
            return new Response('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);
        } catch (\Exception $e){
            return new Response($e->getMessage(), $e->getCode());
        }

        return new Response('<?xml version="1.0"?><result status="OK"></result>', 200);
    }

    /**
     * @Route("/api/credit", name="credit", methods={"POST"})
     */
    public function credit(Request $request, AmountService $service)
    {
        $p = xml_parser_create();
        xml_parse_into_struct($p, $request->getContent(), $data);
        xml_parser_free($p);

        if(!is_array($data) || $data[0]['tag'] != 'CREDIT')
            return new Response('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);

        try{
            $service->credit($data[0]['attributes']);
        } catch (\Exception $e){
            return new Response($e->getMessage(), $e->getCode());
        }

        return new Response('<?xml version="1.0"?><result status="OK"></result>', 200);
    }

    /**
     * @Route("/api/debit", name="debit", methods={"POST"})
     */
    public function debit(Request $request, AmountService $service)
    {
        $p = xml_parser_create();
        xml_parse_into_struct($p, $request->getContent(), $data);
        xml_parser_free($p);

        if(!is_array($data) || $data[0]['tag'] != 'DEBIT')
            return new Response('<?xml version="1.0"?><result status="ERROR" msg="internal server error"></result>', 500);

        try{
            $service->debit($data[0]['attributes']);
        } catch (\Exception $e){
            return new Response($e->getMessage(), $e->getCode());
        }

        return new Response('<?xml version="1.0"?><result status="OK"></result>', 200);
    }
}
